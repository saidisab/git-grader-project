import git
import pandas as pd

def git_log(default_path='.'):
    git_df = pd.DataFrame()
    repo = git.Repo(default_path)
    commits = list(repo.iter_commits("master"))

    authors = []
    dates = []
    hashes = []
    comments = []

    for i in range(len(commits)):
        authors.append(commits[i].author)
        dates.append(commits[i].committed_datetime)
        comments.append(commits[i].message)
        hashes.append(commits[i].hexsha)
    git_df['Author'] = authors
    git_df['Date'] = dates
    git_df['Hash'] = hashes
    git_df['Comment'] = comments
    return git_df

