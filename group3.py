import pandas as pd
from collections import defaultdict
from group2 import git_diff

def def_value():
    return 0

def git_grade(df):
    authors = defaultdict(def_value)
    for i, row in df.iterrows():
        if i == df.shape[0] - 1:
            break
        author_name = row['Author']
        hash1 = row['Hash']
        hash2 = df['Hash'][i+1]
        nlines = git_diff(hash1,hash2)
        authors[author_name] += nlines
            
    return authors
    
