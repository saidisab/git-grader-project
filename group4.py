import matplotlib.pyplot as plt
import numpy as np

def git_graph(authors_table = {"a":10,"b":20,"c":30}, save=False):
    """
    Inputs:
    authors_table - dict w /keys:author names, vals:number of commits
    save - save plot as jpeg, default False
    
    Output:
    None - generates plot
    """
    #taken from: https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
    authors_table_sorted = dict(sorted(authors_table.items(), key=lambda item: item[1],reverse=True))
    names = np.array(list(authors_table_sorted.keys()))
    vals = np.asarray(list(authors_table_sorted.values()))
    
    #taken from: https://stackoverflow.com/questions/64068659/bar-chart-in-matplotlib-using-a-colormap
    my_cmap = plt.get_cmap("viridis")
    rescale = lambda x: (x - np.min(x)) / (np.max(x) - np.min(x))
    
    plt.figure(figsize=(10,5))
    plt.bar(names,vals,color=my_cmap(rescale(vals)))
    plt.title("Git Author Contributions")
    plt.xlabel("Author")
    plt.ylabel("# of Contributions")
    if save:
        plt.savefig('authors.jpeg')
    else:
        plt.show
